function getStorage(data)
{
    if(navigator.userAgent.indexOf('Chrome') > -1)
    {
        return new Promise(function(ok, reject)
        {
            var getting = browser.storage.local.get(data, function(result)
            {
                ok(result);
            });
        });
    }
    else
    {
        return new Promise(function(ok, reject)
        {
            var getting = browser.storage.local.get(data).then(function(result)
            {
                ok(result);
            }, reject);
        });
    }
}

function getBgPage()
{
    return new Promise(function(ok, reject)
    {
        if(navigator.userAgent.indexOf('Chrome') > -1)
        {
            browser.runtime.getBackgroundPage(function(x)
            {
                return ok(x);
            });
        }
        else
        {
            return browser.runtime.getBackgroundPage().then(ok, reject);
        }
    });
}
