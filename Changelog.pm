# 1.14

- Add btc relative assets

# 1.13

- Add ticker badge text colors
- Display % variations too if wanted

# 1.12

- Hide total variations

# 1.11

- Hide number value in ticker

# 1.10

- Do not clear alarm.

# 1.8

- Use browser alarms instead of setTimeouts to trigger update

# 1.7

- Added % variation 24h / week

# 1.6

- Added price/unit and owned value

Added 

# 1.5

- Added force update button
